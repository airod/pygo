# PyGo

PyGo is a module allowing the automatic construction of python objects, from a json, a yaml or simply a dictionary.

The principle is to obtain an object, whose attributes are the representation of the input data.

It is possible to recover in the form of a dictionary all the objects recursively contained in each other

There are three classes for building objects recursively.
- GenericObject: its input structure is similar to a dictionary (dict, json or yaml)
- ListGenericObject: its input structure is a list (either of value, or of list, or of dictionary)
- UniqGenericObject: its input structure is a simple value (string, integer, ...)
## How to use

### Create a new object
```
from PyGo import GenericObject as pygo

JsonDatas = """{
        "title": "example glossary",
		"GlossDiv": {
            "title": "S",
			"GlossList": {
                "GlossEntry": {
                    "ID": "SGML",
					"SortAs": "SGML",
					"GlossTerm": "Standard Generalized Markup Language",
					"Acronym": "SGML",
					"Abbrev": "ISO 8879:1986",
					"GlossDef": {
                        "para": "A meta-markup language, used to create markup languages such as DocBook.",
						"GlossSeeAlso": ["GML", "XML"]
                    },
					"GlossSee": "markup"
                }
            }
        }
    }"""
    
myPygo = pygo(JsonDatas, 'json')

myPygo = pygo(JsonDatas, 'json')

print(myPygo.title)
print(myPygo.GlossDiv)
print(myPygo.GlossDiv.datas())
myPygo.GlossDiv.title.set("new title")
myPygo.title="new title 2"
print(myPygo.GlossDiv.datas())

```
Output

```
<PyGo.UniqGenericObject object at 0x000001F5B4C90F70>
<PyGo.GenericObject object at 0x000001F5B4C90F40>
{'title': 'S', 'GlossList': {'GlossEntry': {'ID': 'SGML', 'SortAs': 'SGML', 'GlossTerm': 'Standard Generalized Markup Language', 'Acronym': 'SGML', 'Abbrev': 'ISO 8879:1986', 'GlossDef': {'para': 'A meta-markup language, used to create markup languages such as DocBook.', 'GlossSeeAlso': ['GML', 'XML']}, 'GlossSee': 'markup'}}}
new title
{'title': 'new title', 'GlossList': {'GlossEntry': {'ID': 'SGML', 'SortAs': 'SGML', 'GlossTerm': 'Standard Generalized Markup Language', 'Acronym': 'SGML', 'Abbrev': 'ISO 8879:1986', 'GlossDef': {'para': 'A meta-markup language, used to create markup languages such as DocBook.', 'GlossSeeAlso': ['GML', 'XML']}, 'GlossSee': 'markup'}}}

```