# from PyGo import GenericObject as pygo

# with open('tests/datasets/datas.yaml', 'r') as File:
#     print('Test from Yaml file')
#     stream = File.read()
#     GO = pygo(stream, 'yaml')
#     print(GO.glos_sary.GlossDiv.GlossList.GlossEntry.GlossDef.GlossSeeAlso.objects[0].value)
#
# with open('tests/datasets/datas.json', 'r') as File:
#     print('Test from Json file')
#     stream = File.read()
#     GO = pygo(stream, 'json')
#     print(GO.glos_sary.GlossDiv.GlossList.GlossEntry.GlossDef.GlossSeeAlso.objects[0].value)
#     print(GO.datas())

from PyGo import GenericObject as pygo

JsonDatas = """{
        "title": "example glossary",
		"GlossDiv": {
            "title": "S",
			"GlossList": {
                "GlossEntry": {
                    "ID": "SGML",
					"SortAs": "SGML",
					"GlossTerm": "Standard Generalized Markup Language",
					"Acronym": "SGML",
					"Abbrev": "ISO 8879:1986",
					"GlossDef": {
                        "para": "A meta-markup language, used to create markup languages such as DocBook.",
						"GlossSeeAlso": ["GML", "XML"]
                    },
					"GlossSee": "markup"
                }
            }
        }
    }"""

myPygo = pygo(JsonDatas, 'json')

print(myPygo.title)
print(myPygo.GlossDiv)
print(myPygo.GlossDiv.datas())
myPygo.GlossDiv.title.value="new title"
print(myPygo.GlossDiv.title.value)
myPygo.title="new title 2"
print(myPygo.GlossDiv.datas())
print(myPygo.datas())

