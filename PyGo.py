import json, yaml


class GenericObject(object):
    def __init__(self, datas, datatype: str, exclude_key: list=[], format_map: dict={}):

        if type(datas) == type(dict()):
            self._init_datas = datas
        elif datatype in ('json', 'yaml'):
            try:
                if datatype == 'json':
                    self._init_datas = json.loads(datas)
                elif datatype == 'yaml':
                    self._init_datas = yaml.safe_load(datas)
            except Exception as e:
                raise TypeError('Datas not supported : %s' % (str(e)))
        else:
            raise TypeError('Type of datas not supported')
        self._type = datatype
        self._exclude_key = exclude_key
        # self.datas = self._init_datas.copy()
        self.__auto_construct__()

    def __auto_construct__(self):
        """internal method : contruct of datas"""
        for _key, _value in self._init_datas.items():
            if _key in self._exclude_key: continue
            k, v = self.__load__(_key, _value)
        delattr(self, "_init_datas")

    def __load__(self, _key: str, _value) -> any:
        """internal method : loader of datas"""
        key = str(_key).replace(' ', '_')  #evite de créer des attributs avec des espaces
        if type(_value) == type(list()):
            self.__dict__[key] = ListGenericObject(_value)
        elif type(_value) == type(dict()):
            self.__dict__[key] = GenericObject(_value, datatype='dict')
        else:
            self.__dict__[key] = UniqGenericObject(_value)
        return (key, self.__dict__[key])

    def get(self, key: str) -> any:
        """return value of attribute if exist, or return None"""
        return self.__dict__.get(key)

    def set(self, key, value) -> tuple:
        """Add or modify the value of attribute"""
        return self.__load__(key, value)

    def datas(self) -> dict:
        """returns a dictionary of the values of all attributes"""
        dict_datas = {}
        for k, v in self.__dict__.items():
            if k.startswith('_'):continue
            if type(v) == ListGenericObject:
                dict_datas[k] = v.data_list()
            elif type(v) == GenericObject:
                dict_datas[k] = v.datas()
            elif type(v) == UniqGenericObject:
                dict_datas[k] = v.value
            else:
                dict_datas[k] = v
        return dict_datas


class ListGenericObject(object):
    def __init__(self, liste_of_value):
        self._value = liste_of_value
        self.objects = []
        self.__auto_construct__()

    def __auto_construct__(self):
        """internal method : contruct of datas"""
        for item in self._value:
            self.__load__(item)

    def __load__(self, item):
        """internal method : loader of datas"""
        if type(item) == type(list()):
            self.objects.append(ListGenericObject(item))
        elif type(item) == type(dict()):
            self.objects.append(GenericObject(item, datatype='dict'))
        else:
            self.objects.append(UniqGenericObject(item))

    def data_list(self) -> list:
        """returns a list of the values of all attributes"""
        item_list = []
        for v in self.objects:
            if type(v) == ListGenericObject:
                item_list.append(v.data_list())
            elif type(v) == GenericObject:
                item_list.append(v.datas())
            elif type(v) == UniqGenericObject:
                item_list.append(v.value)
            else:
                item_list.append(v)
        return item_list

    def set(self, value):
        """Add a new object"""
        self.objects.append(self.__load__(value))

    def get(self, index):
        """returns the "index" item of the object if it exists, otherwise throws an exception"""
        try:
            return self.objects[index]
        except IndexError as e:
            raise IndexError('Not Found : list index out of range')


class UniqGenericObject(object):
    def __init__(self, value):
        self.value = value
        self._format()

    def _format(self):
        """todo : format value with pattern"""
        pass



